-- MySQL dump 10.16  Distrib 10.3.8-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: d_task_tracker_adv
-- ------------------------------------------------------
-- Server version	10.3.8-MariaDB-1:10.3.8+maria~bionic-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'Admin','whichUserGroup',NULL,1532280181,1532280181),('assignee',1,'Assignee','whichUserGroup',NULL,1532280182,1532280182),('createProfile',2,'Create profile',NULL,NULL,1532280181,1532280181),('createProject',2,'Create project',NULL,NULL,1532280181,1532280181),('createTask',2,'Create task',NULL,NULL,1532280181,1532280181),('createTeam',2,'Create Team',NULL,NULL,1532280181,1532280181),('createUser',2,'Create user',NULL,NULL,1532280181,1532280181),('deleteProfile',2,'Delete profile',NULL,NULL,1532280181,1532280181),('deleteProject',2,'Delete project',NULL,NULL,1532280181,1532280181),('deleteTask',2,'Delete task',NULL,NULL,1532280181,1532280181),('deleteTeam',2,'Delete Team',NULL,NULL,1532280181,1532280181),('deleteUser',2,'Delete user',NULL,NULL,1532280181,1532280181),('readProfile',2,'Read user`s profile',NULL,NULL,1532280181,1532280181),('readProject',2,'Read project description',NULL,NULL,1532280181,1532280181),('readTask',2,'Read task description',NULL,NULL,1532280181,1532280181),('readTeam',2,'Read Team`s data',NULL,NULL,1532280181,1532280181),('readUser',2,'Read user`s data',NULL,NULL,1532280181,1532280181),('teamlead',1,'Teamlead','whichUserGroup',NULL,1532280181,1532280181),('updateOwnProfile',2,'Update own profile','isProfileOwner',NULL,1532280181,1532280181),('updateProfile',2,'Update profile',NULL,NULL,1532280181,1532280181),('updateProject',2,'Update project',NULL,NULL,1532280181,1532280181),('updateTask',2,'Update task',NULL,NULL,1532280181,1532280181),('updateTeam',2,'Update Team`s data',NULL,NULL,1532280181,1532280181),('updateUser',2,'Update user`s data',NULL,NULL,1532280181,1532280181),('user',1,'User','whichUserGroup',NULL,1532280182,1532280182);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','createProfile'),('admin','createProject'),('admin','createTask'),('admin','createTeam'),('admin','createUser'),('admin','deleteProfile'),('admin','deleteProject'),('admin','deleteTask'),('admin','deleteTeam'),('admin','deleteUser'),('admin','readProfile'),('admin','readProject'),('admin','readTask'),('admin','readTeam'),('admin','readUser'),('admin','updateProfile'),('admin','updateProject'),('admin','updateTask'),('admin','updateTeam'),('admin','updateUser'),('assignee','createProfile'),('assignee','readProfile'),('assignee','readProject'),('assignee','readTask'),('assignee','readTeam'),('assignee','readUser'),('assignee','updateOwnProfile'),('teamlead','createProfile'),('teamlead','createProject'),('teamlead','createTask'),('teamlead','createTeam'),('teamlead','createUser'),('teamlead','deleteProfile'),('teamlead','deleteProject'),('teamlead','deleteTask'),('teamlead','deleteTeam'),('teamlead','deleteUser'),('teamlead','readProfile'),('teamlead','readProject'),('teamlead','readTask'),('teamlead','readTeam'),('teamlead','readUser'),('teamlead','updateProfile'),('teamlead','updateProject'),('teamlead','updateTask'),('teamlead','updateTeam'),('teamlead','updateUser'),('updateOwnProfile','updateProfile'),('user','readProject'),('user','readTask');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('isProfileOwner','O:23:\"common\\rbac\\ProfileRule\":3:{s:4:\"name\";s:14:\"isProfileOwner\";s:9:\"createdAt\";i:1532280181;s:9:\"updatedAt\";i:1532280181;}',1532280181,1532280181),('whichUserGroup','O:25:\"common\\rbac\\UserGroupRule\":3:{s:4:\"name\";s:14:\"whichUserGroup\";s:9:\"createdAt\";i:1532280181;s:9:\"updatedAt\";i:1532280181;}',1532280181,1532280181);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) DEFAULT 10,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment__task_idx` (`task_id`),
  CONSTRAINT `fk_comment__task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,6,'Hello!\r\nI made it - `Introduce new local schema for project`.\r\n\r\nNel',10,'2018-06-29 13:01:10','2018-06-29 13:01:10'),(2,6,'Hello!\r\n  This is a difficult task can you make a clue for me.\r\nDarn Nel',10,'2018-06-29 13:44:10','2018-06-29 13:44:10'),(3,13,'There is wow task. ',NULL,'2018-07-06 19:46:31','2018-07-06 19:46:31'),(4,14,'This is very difficult task for me.',NULL,'2018-07-24 17:06:36','2018-07-24 17:06:36');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,'admin','Powerful admin','2018-07-03 10:23:33','2018-07-03 10:23:36'),(2,'teamlead','Powerful teamlead','2018-07-03 10:25:58','2018-07-03 10:26:00'),(3,'assignee','Professional assignee','2018-07-03 10:27:42','2018-07-03 10:27:45'),(4,'user','Simple user','2018-07-03 10:28:17','2018-07-03 10:28:20');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('common\\migrations\\m180626_174523_create_task_table',1530027016),('common\\migrations\\M180629112806_create_users_profile_table',1530273973),('common\\migrations\\M180629142010Add_column_status_to_users_profile_table',1530283378),('common\\migrations\\M180703064257Create_group_table',1530602035),('common\\migrations\\M180703064324Create_team_table',1530602035),('common\\migrations\\M180703064403Create_user_team_group_table',1530602037),('common\\migrations\\M180708180100Create_status_table',1531075571),('common\\migrations\\M180708180302Create_project_table',1531075573),('common\\migrations\\M180708183205Add_column_project_id_to_task',1531078550),('common\\migrations\\M180708191150Add_column_project_title_to_project_table',1531078550),('common\\migrations\\M180716175429Create_token_table',1531763793),('frontend\\migrations\\M180628180246_create_comment_table',1530209573),('m000000_000000_base',1529754323),('m130524_201442_init',1529754332),('m140506_102106_rbac_init',1530552867),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1530552868);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamlead_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT 10,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `project_title` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_project__user_idx` (`teamlead_id`),
  KEY `fk_project__status_idx` (`status_id`),
  CONSTRAINT `fk_project__status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  CONSTRAINT `fk_project__user` FOREIGN KEY (`teamlead_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (2,3,10,'E-commerce `Tbag` web service.','2018-05-01 21:52:46','2018-09-08 21:53:08',NULL,3,3,'2018-05-01 21:52:46','2018-07-22 20:26:32','E-commerce `Tbag`'),(3,4,10,'Big Data collecting project.','2018-07-10 12:00:00','2018-10-10 12:00:00',NULL,4,3,'2018-07-10 09:59:11','2018-07-22 20:26:51','Big Data collecting'),(4,5,10,'E-commerce clothing fashion store','2018-07-20 00:00:00','2018-09-01 00:00:00',NULL,5,5,'2018-07-20 12:47:27','2018-07-20 12:47:27','E-commerce clothing fashion store'),(6,6,10,'New town`s site with news, and more...','2018-07-20 00:00:00','2018-10-01 00:00:00',NULL,4,4,'2018-07-20 18:53:21','2018-07-21 10:21:38','New town`s site with news');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Deteled'),(2,'Done'),(3,'Cancelled'),(4,'Testing'),(5,'Bug fixing'),(6,'Reviewing'),(7,'Supporting'),(8,'Pending'),(9,' On consideration'),(10,'Active');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(124) COLLATE utf8_unicode_ci NOT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `teamlead_id` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `deadline` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `status` tinyint(3) DEFAULT 0,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `project_id` int(11) DEFAULT 2,
  PRIMARY KEY (`id`),
  KEY `fk_task__user_assignee_idx` (`assignee_id`),
  KEY `fk_task__user_teamlead_idx` (`teamlead_id`),
  KEY `fk_task__project_idx` (`project_id`),
  CONSTRAINT `fk_task__project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `fk_task__user_assignee` FOREIGN KEY (`assignee_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_task__user_teamlead` FOREIGN KEY (`teamlead_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,'Reintermediate front-end web services',11,2,'2018-06-20 10:00:00','2018-07-20 10:00:00',NULL,10,'Energize compelling paradigms. Start new level web services. Search new paradigms.','2018-06-19 19:45:48','2018-06-26 19:45:52',2),(2,'	Innovate B2C platforms',10,2,'2018-06-10 10:00:00','2018-08-10 10:00:00','2018-07-23 10:00:00',2,'Unleash user-centric relationships. New relationships. Invent some new.','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(3,'Maximize open-source niches',9,2,'2018-06-11 10:00:00','2018-09-11 10:00:00','2018-07-24 10:00:00',2,'Maximize front-end ROI','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(4,'Aggregate synergistic vortals',8,2,'2018-06-12 10:00:00','2018-08-12 10:00:00',NULL,10,'Brand e-business synergies','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(5,'Evolve global schemas',7,3,'2018-05-13 10:00:00','2018-06-28 10:00:00',NULL,10,'Enable leading-edge technologies','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(6,'Intreduce local schemas',7,3,'2018-06-23 10:00:00','2018-07-23 10:00:00',NULL,10,'Intreduce local schemas with new future','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(7,'Disintermediate world-class experiences',6,3,'2018-06-24 10:00:00','2018-08-24 10:00:00',NULL,10,'Utilize dynamic e-services','2018-06-09 19:45:48','2018-06-26 19:45:52',2),(8,'Engineer rich interfaces',5,4,'2018-06-25 10:00:00','2018-07-24 10:00:00',NULL,10,'Exploit back-end partnerships','2018-06-09 19:45:48','2018-06-26 19:45:52',3),(9,'Mesh enterprise e-markets',12,4,'2018-06-24 10:00:00','2018-08-24 10:00:00',NULL,10,'Syndicate ubiquitous paradigms','2018-06-09 19:45:48','2018-06-26 19:45:52',3),(10,'Strategize seamless metrics',12,4,'2018-08-24 10:00:00','2018-09-24 10:00:00',NULL,10,'Leverage one-to-one bandwidth','2018-06-09 19:45:48','2018-06-26 19:45:52',3),(11,'Monetize transparent paradigms',13,6,'2018-06-20 10:00:00','2018-09-20 10:00:00',NULL,10,'Drive transparent partnerships','2018-06-09 19:45:48','2018-06-26 19:45:52',6),(12,'Clean mass',14,6,'2018-06-20 10:00:00','2018-07-20 10:00:00',NULL,10,'Drive partnerships','2018-06-09 19:45:48','2018-06-26 19:45:52',6),(13,'Aggregate synergistic matter',15,6,'2018-07-01 10:00:00','2018-08-20 10:00:00',NULL,10,'E-business synergies','2018-06-09 19:45:48','2018-06-26 19:45:52',6),(14,'Calculate current project risks',15,3,'2018-07-24 00:00:00','2018-07-31 00:00:00',NULL,10,'Calculate risks for E-commerce `Tbag`','2018-07-24 17:02:41','2018-07-24 17:02:41',2);
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Big web','Web developers','2018-07-03 10:18:44','2018-07-03 10:18:59'),(2,'Intelligent soft','Soft developers','2018-07-03 10:21:30','2018-07-03 10:21:33'),(3,'Driving mobile','Mobile developres','2018-07-03 10:22:19','2018-07-03 10:22:22'),(4,'Administrators','Administrators','2018-07-03 10:31:51','2018-07-03 10:31:54'),(5,'Newcomers','Default for new users','2018-07-04 17:46:43','2018-07-04 17:46:45'),(6,'Intelligent development','Intelligent development','2018-07-04 17:46:43','2018-07-04 17:46:45');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expired_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `idx-token-user_id` (`user_id`),
  CONSTRAINT `fk-token-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,1,'BW15e9iyqQvjBxVD9ZIEie_WFSd3nOO4',1532070358);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','tfDMWhI4zoMaeLZcCudZJPEGeM6B57w5','$2y$13$p9/2VJsBO8mjTvMaLwEF9.lp5EDTRQSG4YueQEtGQV30tYdOX29mu',NULL,'admin@tt.com',10,1530723505,1530723505),(2,'adlai_ludovici','OWxUPCLXPSx4mAxkh4qJ7rOJYq0pJUpj','$2y$13$sXc0F0pZ41vIJ3lumomYPuE57s1/T.9dOF7.MsF5BtO39X1lJ2CJu',NULL,'aludovici0@tt.org',10,1530724575,1530724575),(3,'valle_tetford','58qJ9ayK0bR8iAOS6uJfbzgrD__qDOvD','$2y$13$fIJiIWeFb8TDiW98lnKOEONB6j5.b2vSqg7IeBGhxLiJNtKkcgVme',NULL,'vtetford1@umich.edu',10,1530725076,1530725076),(4,'valencia_sewell','B8UTx2T3Me66mcvXsT3vlcXhjDrew3Y-','$2y$13$uvULtoFLQiwhKfkiICgxX.tW15zs5K5b7Nu9O/ze0GCS6UUPN/DKS',NULL,'vsewell2@google.com.br',10,1530729422,1530729422),(5,'nonna_axup','uTI2YsAZ4AvhiYxRPbyoaFnMS9U8HEmV','$2y$13$yBS7jPK2Uwn2/dCe.fUmNO7Op7gNWctMTSYYDbgceZO2T5Eeyjsb.',NULL,'naxup3@fda.org',10,1530731475,1530731475),(6,'darn_nel','lLb40zqYndUYIVyn-RREAnBUZxYDaeCA','$2y$13$7ROJXpb6mWj.rO.IFktoCeJ3gCJ9Zxz5sSBC0I6Xyjnd6rFS4OonK',NULL,'dnel4@adobe.com',10,1530733282,1530733282),(7,'tiffany_clace','7crXzkNhu1nv22QAi1SD8OkXO5NmRjYh','$2y$13$/CQgEWghaKPHnV7eH.JlkeSFPumF6/XJ8/sDsxwq33Pq6hB.Y/sHO',NULL,'tclace5@mayoclinic.com',10,1530735345,1530735345),(8,'rafaello_coe','nnHDUpU4iza9L45wCyJCPlQau-QzVa5e','$2y$13$PjAZS6ya1AQNUg7xedReduvlo5UHBiXXHFP/XEC5kQNzLf3Qk1BTe',NULL,'rcoe6@paypal.com',10,1530735481,1530735481),(9,'emalia_wattam','uSBkyWk9gXEYaZU1cJ_7XQIYgVZS__kd','$2y$13$1Kdtks6KphyNTf7.Lu04bekTrbYXNIFKH4h/S4DHgf2Se7QzDYgRm',NULL,'ewattam7@google.cn',10,1530769773,1530769773),(10,'marjie_laurant','3mbCDA6JRhA4BUR9nQvGDbqE3bw2evbW','$2y$13$LBxPi8i66NqISiFN605Rru6vDtvcvtWGpxFEuq192m8qyy50l/WgC',NULL,'mlaurant8@microsoft.com',10,1530770013,1530770013),(11,'benita_kindred','5X67RxCsm8xWQKN49pGmLb8YBNNdTvqZ','$2y$13$T8Itul.3.Gti.n7tWixpyOd29QwUaCUbuAZywrBLcGJvvOSuEV3xK',NULL,'bkindred9@bing.com',10,1530770476,1530770476),(12,'kylan_bennett','RRZF3ZYtYUnogIEaczTPuQfm8s4M2_tc','$2y$13$r/uo2bV5hHyOczixEF.Fq.lNTVwoIFbRWbh823xm.//uAm.uYy4Xi',NULL,'kylan_bennett@tt.org',10,1530772502,1530772502),(13,'martina_shepard','2bMsWZGOSGWV4ESFl8ShKLB_o9E-SBZU','$2y$13$NoRpugBk1YkiiPmJ1dQiyubww84e54D6LNj7HQEmqWx6R6T.ZJcFW',NULL,'martina_shepard@tt.org',10,1530772628,1530772628),(14,'cade_mcguire','SeZ1dfPZj5BpwHliEZ8nIQiuWHsga_rD','$2y$13$6hgFldkIeMqwhcp9MeQsTepaP3bf.K3ZoV6zQmGxgR/OFDZUtYWUC',NULL,'cade_mcguire@tt.org',10,1530772720,1530772720),(15,'alfonso_brennan','PKs6WCjKfl5ud7ZNkifXWPaHPkgrq3pr','$2y$13$l98msct6c9VL9Ic8UM5jvO3nIR96jO5BrfNwJR04793oddGZ7KpGC',NULL,'alfonso_brennan@tt.org',10,1530773470,1530773470),(16,'john_maickom','8nlp4g2hy2z8rNafaltpCCrHRSQB2IqL','$2y$13$2beFXTq6WcnkEBI.qHHHIOXC3aqE1EOvikwq6CwPuLwZ0Mz8lRmqm',NULL,'john_maickom@tt.org',10,1531153074,1531153074),(17,'ivan_monove','8cJi27vKtgXlhQ5IDbx2m6rFZSxSjbBK','$2y$13$npBfqLf1DuaXa4sdeWJf3eCfAM.UEak/Fy3ujPkeQvDc5ACtaQ7du',NULL,'ivan_monove@tt.org',10,1531153600,1531156293),(18,'robin_sanders','hzk2KwUaK9oRcreFCMWwsqOnGVrU4Dno','$2y$13$q8qr5uyHwXZ.JrXQJHrZNOa0jhA2QUZzF6xMvpM9hEURn8vuYXiwi',NULL,'robin_sanders@tt.org',10,1532283795,1532285013);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_team_group`
--

DROP TABLE IF EXISTS `user_team_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_team_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user__user_team_group_idx` (`user_id`),
  KEY `fk_group__user_team_group_idx` (`group_id`),
  KEY `fk_team__user_team_group_idx` (`team_id`),
  CONSTRAINT `fk_group__user_team_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `fk_team__user_team_group` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  CONSTRAINT `fk_user__user_team_group` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_team_group`
--

LOCK TABLES `user_team_group` WRITE;
/*!40000 ALTER TABLE `user_team_group` DISABLE KEYS */;
INSERT INTO `user_team_group` VALUES (1,1,1,4),(2,2,1,4),(3,3,2,1),(4,4,2,2),(5,5,2,3),(6,6,2,6),(7,7,3,1),(8,8,3,1),(9,9,3,2),(10,10,3,2),(11,11,3,2),(12,12,3,3),(13,13,3,3),(14,14,3,3),(15,9,3,6),(16,10,3,6),(17,11,3,6),(18,12,3,6),(19,13,3,6),(20,14,3,6),(21,15,3,1),(22,16,4,5),(23,17,4,5),(24,18,4,5);
/*!40000 ALTER TABLE `user_team_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_profile`
--

DROP TABLE IF EXISTS `users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(124) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(124) COLLATE utf8_unicode_ci DEFAULT NULL,
  `specialization` varchar(124) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` tinyint(3) DEFAULT 1,
  `birthday` date DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'Russia',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) DEFAULT 10,
  PRIMARY KEY (`id`),
  KEY `fk_user__users_profile_idx` (`user_id`),
  CONSTRAINT `fk_user__users_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_profile`
--

LOCK TABLES `users_profile` WRITE;
/*!40000 ALTER TABLE `users_profile` DISABLE KEYS */;
INSERT INTO `users_profile` VALUES (1,11,'Benita','Kindred','Web developer',0,'1970-05-12','+11-810-452-45-78','01_women.jpg','UK','2018-06-29 20:04:04','2018-07-21 11:59:21',10),(2,3,'Valle','Tetford','Web developer. Frontend',0,'1978-06-17','+4-510-752-41-72','05_women.jpg','Czech Republic','2018-07-01 15:59:06','2018-07-24 13:14:24',10),(3,14,'Cade','Mcguire','Web developer',1,'1986-06-12','+06-914-501-48-34','04_man.jpg','France','2018-07-05 09:38:40','2018-07-05 09:38:40',10),(4,15,'Alfonso','Brennan','Web developer',1,'1979-04-11','+04-651-721-06-01','06_man.jpg','Italy','2018-07-05 09:51:10','2018-07-24 14:13:47',10),(5,16,'John','Maickom','Web developer',1,'1983-05-18','+07-985-701-26-93','07_man.jpg','Russia','2018-07-09 19:17:54','2018-07-09 19:17:54',10),(6,17,'Ivan','Monove','Web backend',1,'1984-06-19','+09-400-708-30-01','08_man.jpg','Czech Republic','2018-07-09 19:26:40','2018-07-19 18:44:49',10),(7,4,'Valencia','Sewell','Web developer',1,'1985-01-18','+03-416-201-90-01','11_women.jpg','UK','2018-07-09 19:26:40','2018-07-09 19:26:40',10),(8,5,'Nonna','Axup','Mobile developer',1,'1981-05-07','+06-904-201-08-04','12_women.jpg','France','2018-07-19 17:04:45','2018-07-19 17:04:45',10),(9,6,'Darn','Nel','Soft developer',1,'1985-03-14','+07-931-270-04-11','09_man.jpg','Russia','2018-07-19 17:22:38','2018-07-19 17:22:38',10),(10,7,'Tiffany','Clace','Soft developer',1,'1983-06-17','+11-901-738-40-81','02_man.jpg','Germany','2018-07-19 17:28:00','2018-07-19 17:28:00',10),(11,1,'Admin','Adminovich','Maga Admin',1,'1979-04-15','+07-968-501-03-09','10_man.jpg','Russia','2018-07-21 20:26:50','2018-07-21 20:26:50',10),(12,2,'Adlai','Ludovici','Web developer',1,'1981-06-23','+12-820-752-45-79','03_man.jpg','Czech Republic','2018-06-29 20:04:04','2018-07-21 11:59:21',10),(13,9,'Emali','Wattam','Web developer',1,'1989-07-25','+07-968-471-03-09','10_man.jpg','Russia','2018-07-22 20:29:52','2018-07-22 20:29:52',10),(14,18,'Robin','Sanders','Web developer',1,'1982-02-24','+07-968-841-63-09','10_man.jpg','Russia','2018-07-22 20:29:52','2018-07-22 20:29:52',10),(15,13,'Martin','Shepard','Mobile developer',1,'1988-06-17','+09-104-560-09-11','08_man.jpg','France','2018-07-23 19:19:42','2018-07-23 19:19:42',10),(16,12,'Kyla','Bennett','Web developer',1,'1981-12-15','+06-921-400-70-09','10_man.jpg','Belgium','2018-07-23 20:57:21','2018-07-23 21:22:46',10),(17,10,'Marji','Laurant','Software engineer',1,'1983-03-21','+04-901-501-30-71','10_man.jpg','France','2018-07-23 21:27:29','2018-07-23 21:29:12',10),(18,8,'Rafaello','Coe','Software developer',1,'1986-06-12','+05-903-780-90-11','10_man.jpg','Italy','2018-07-24 18:58:01','2018-07-24 19:01:54',10);
/*!40000 ALTER TABLE `users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'd_task_tracker_adv'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-25 15:27:43
